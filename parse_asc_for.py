#!/usr/bin/env python3
'''Parse ASC file using for/break'''

fname = "AY38_G6_0.5_pH-3.50004_0001.ASC"

from pprint import pprint

fd = open(fname, encoding="latin1")
title = next(fd).strip() # or f.readline().strip() 

print('Title:', title)

# Metadata
meta = {}

for current in fd:
    current = current.strip()
    if current == '':
        break # END OF HEADER
    name, value = current.split(':', 1) # one cut (2 parts)
    name = name.strip()
    value = value.strip()
    if '"' in value:
        value = value.replace('"', '')
        if value == '':
            continue # not considering empty values
    else:
        value = float(value)
    meta[name] = value

pprint(meta)

if fd.readline().strip() == '"Correlation"':
    print("Processing Correlation")
    correlation = []
    for current in fd:
        current = current.strip()
        if current == '':
            break # END OF MATRIX
        row = [ float(v) for v in current.split() ]
        correlation.append(row)
    
    pprint(correlation)

