#!/usr/bin/env python3

from random import randint

def asknumber(text):
    ok = False
    while not ok:
        answer = input('Number: ').strip()
        if answer.isnumeric():
            ok = True
    return int(answer)

secret = randint(1, 100)

print('I picked a random number between 1 and 100.')

while True:
    guess = asknumber('Your guess: ')
    if guess > secret:
        print('Too big.')
    elif guess < secret:
        print('Too small.')
    else: # equal
        print('Congratulation.')
        break

