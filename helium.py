#!/usr/bin/env python3

from atom import Atom

He4 = Atom('Helium', 'He', 2, 4)
He3 = Atom('Helium', 'He', 2, 3)

all  = [ He4, He3 ]

# let's loop through all instances
for atom in all:
    print(atom.name)
    print(atom) # here str is called
    print(f"  repr : ", repr(atom))
    print(f"  Mass number:    {atom.A}")
    print(f"  Atomic number:  {atom.Z}")
    print(f"  Neutron number: {atom.neutron_number()}")
