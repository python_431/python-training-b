#!/usr/bin/env python3

from random import randint

secret = randint(1, 100)

print('I picked a random number between 1 and 100.')

while True:
    guess = int(input('Your guess: ').strip())
    if guess > secret:
        print('Too big.')
    elif guess < secret:
        print('Too small.')
    else: # equal
        print('Congratulation.')
        break

