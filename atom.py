#!/usr/bin/env python3

# Class definition

class Atom:
    def __init__(self, name, symbol, Z, A):
        self.name = name
        self.symbol = symbol
        self.Z = Z # 'Atomic number' like in Ztom, sigh.
        self.A = A # 'Mass number'
    def neutron_number(self):
        return self.A - self.Z # right ?
    def electron_number(self):
        return self.Z
    def charge(self):
        return -self.electron_number() + self.Z
    def __str__(self):
        return f"({self.A}){self.symbol}"
    def __repr__(self):
        return f"Atom({repr(self.name)}, {repr(self.symbol)}, {repr(self.Z)}, {repr(self.A)})"

class Ion(Atom):
    def __init__(self, name, symbol, Z, A, e_deficit):
        self.e_deficit = e_deficit
        super().__init__(name, symbol, Z, A)
    def electron_number(self):
        return self.Z - self.e_deficit
    # no need to redefine/override charge (because it is calling electron_number)
    def __str__(self):
        return super().__str__() + \
                (f"{abs(self.e_deficit)}" if abs(self.e_deficit) > 1 else '') + \
                ('+' if self.e_deficit > 0 else '-') # assuming it is not 0
    def __repr__(self):
        return f"Ion({repr(self.name)}, {repr(self.symbol)}, {repr(self.Z)}, {repr(self.A)},{repr(self.e_deficit)})"

# test code

# A few instances
if __name__ == '__main__': # True if I'm run as a program
                           # False if imported
    He4 = Atom('Helium', 'He', 2, 4)
    He3 = Atom('Helium', 'He', 2, 3)

    H = Atom('Hydrogen', 'H', 1, 1)

    Ca12 = Atom('Carbon', 'Ca', 6, 12)
    Ca14 = Atom('Carbon', 'Ca', 6, 14)

    # hplus (H+)
    hplus = Ion('Hydrogen','H', 1, 1, 1)
    # Calcium Ca2+
    calcium = Ion('Calcium', 'Ca', 20, 40, 2)
    # O^{2}- Oxide
    oxide = Ion('Oxide','O', 8, 16, -2)

    all  = [ He4, He3, H, Ca12, Ca14, hplus, calcium, oxide ]

# let's loop through all instances
    for p in all:
        print(p.name)
        print(p) # here str is called
        print(f"  repr : ", repr(p))
        print(f"  Mass number:     {p.A:2}")
        print(f"  Atomic number:   {p.Z:2}")
        print(f"  Neutron number:  {p.neutron_number():2}")
        print(f"  Electron number: {p.electron_number():2}")
        print(f"           Charge: {p.charge():2}")
