#!/usr/bin/env python3

from pprint import pprint

fname = 'data.txt'

fd = open(fname)

#for line in fd:
#    print(line.strip())

data = {}
for line in fd:
    name, value = line.strip().split(':')
    name  = name.strip() # remove spaces around
    data[name] = float(value) # convert into number

pprint(data)
