#!/usr/bin/env python3
'''Parse ASC file using while/walrus operator'''

fname = "AY38_G6_0.5_pH-3.50004_0001.ASC"

from pprint import pprint
import csv

#fd = open(fname, encoding="latin1")

try:
    with open(fname, encoding="ISO8859") as fd:
        title = next(fd).strip() # or f.readline().strip() 
        
        # Metadata
        # new in py 3.7 : 'walrus' operator (:=) it is assigning a value
        # and is an expression having that value
        # before you would have used while True: and break
        meta = {}
        
        
        while ( current := fd.readline().strip() ) != '':
            name, value = current.split(':', 1)
            name = name.strip()
            value = value.strip()
            if '"' in value:
                value = value.replace('"', '')
                if value == '':
                    continue # not considering empty values
            else:
                value = float(value)
            meta[name] = value
        
        pprint(meta)
        
        if fd.readline().strip() == '"Correlation"':
            print("Processing Correlation")
            correlation = []
            while ( current := fd.readline().strip() ) != '':
                row = [ float(v) for v in current.split() ]
                correlation.append(row)
            
            pprint(correlation)
            print('Saving correlation data in correlation.csv')
            # Hint (look at csv module doc) when WRITING
            # a csv file on Windows (it doesn't matter on UNIX)
            # you should use this to not end up with weird ^M
            # characters in the output file
            with open('correlation.csv', 'w', newline='') as csvfile:
                wrt = csv.writer(csvfile)
                wrt.writerows(correlation)
                # or line by line
                # if correlation was a lazy iterators
                # for line in correlation:
                #     wrt.writerow(line)

except (FileNotFoundError, PermissionError, IsADirectoryError) as e:
    print(f'Cannot open {e.filename}: {e.strerror}')
